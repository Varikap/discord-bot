const Discord = require('discord.js');
const PirateBay = require('thepiratebay');
const ytdl = require('ytdl-core');
const CODAPI = require('call-of-duty-api')({ platform: "battle" });
const client = new Discord.Client();
client.disabledMembers = new Map();
require('dotenv').config()
const request = require('request-json');
const token = process.env.DISCORD_TOKEN;
const queue = new Map();

const vote = require('./vote')
const teams = require('./teams')
const csgo = require('./csgoStats')

client.on('ready', () => {
    console.log("Connected as " + client.user.tag);

    client.user.setActivity("with javascript");
});

client.on('message', (recievedMessage) => {
    if (recievedMessage.author == client.user) {
        return;
    }

    if (recievedMessage.content.startsWith("!")) {
        processCommand(recievedMessage);
    }
});

function processCommand(recievedMessage) {
    let fullCommand = recievedMessage.content.substr(1);
    let splitCommand = fullCommand.split(" ");
    let primaryCommand = splitCommand[0];
    let arguments = splitCommand.slice(1);

    const serverQueue = queue.get(recievedMessage.guild.id);

    switch(primaryCommand) {
        case "help":
            helpCommand(arguments, recievedMessage);
            break;
        case "chuck":
            chuckCommand(recievedMessage);
            break;
        case "joke":
            jokeCommand(recievedMessage);
            break;
        case "roll":
            randomNumberCommand(recievedMessage);
            break;
        case "toss":
            coinTossCommand(recievedMessage);
            break;
        case "movie":
            reccomendMovieCommand(recievedMessage, arguments);
            break;
        case "meme":
            meme(arguments, recievedMessage);
            break;
        case "tpb":
            recievedMessage.channel.send('broken... under construction');
            // tpb(arguments, recievedMessage);
            break;
        case "play":      
            recievedMessage.channel.send('broken... under construction');
            // exec(arguments, recievedMessage, serverQueue)
            break;
        case "stop":
            recievedMessage.channel.send('broken... under construction');
            // stop(recievedMessage, serverQueue);
            break;
        case "skip":
            recievedMessage.channel.send('broken... under construction');
            // skip(recievedMessage, serverQueue);
            break;
        case "warzone":
            warzone(arguments, recievedMessage);
            break;
        case "vote":
            vote.vote(arguments, recievedMessage)
            break
        case "teams":
            teams.teams(recievedMessage)
            break
        case "csgo":
            csgo.getStats(recievedMessage, arguments[0])
            break
        default:
            recievedMessage.channel.send("Unknown command. Try \`!help\` .")
    }
}


function chuckCommand(recievedMessage) {
    try{
        const url = "http://api.icndb.com/jokes/";
        const client = request.createClient(url);

        client.get("random", (err, res, body) => {
            recievedMessage.channel.send(body.value.joke);
        });
    } catch(e) {
        console.error(e)
    }
}

function jokeCommand(recievedMessage) {
    try{
        const url = "https://icanhazdadjoke.com";
        const client = request.createClient(url);
        client.headers["Accept"] = "application/json";

        client.get("", (err, res, body) => {
            recievedMessage.channel.send(body.joke); 
        });
    } catch(e) {
        console.error(e)
    }
}

function randomNumberCommand(recievedMessage) {
    try{
        const value = getRandomNumber(1,100);
        recievedMessage.channel.send(value);
    } catch(e) {
        console.error(e)
    }
}

function coinTossCommand(recievedMessage) {
    try{
        const value = getRandomNumber(1,2);
        if (value == 1) {
            recievedMessage.channel.send("heads");
        } else {
            recievedMessage.channel.send("tails");
        }
    } catch(e) {
        console.error(e)
    }
}

function reccomendMovieCommand(recievedMessage, args) {
    try{
        const url = `https://api.themoviedb.org/3/movie/${getRandomNumber(3,2500)}/recommendations?api_key=${process.env.MOVIE_API_KEY}&language=en-US&page=1`;
        const client = request.createClient(url);

        client.get("", (err, res, body) => {
            try{
                const prvi = body.results[getRandomNumber(0,5)];
                recievedMessage.channel.send(`Title: ${prvi.original_title} \n\n Description: ${prvi.overview} \n\n Release Date: ${prvi.release_date}`);
            } catch (err) {
                recievedMessage.channel.send("Can't find movie, please try again.");
            }
        });
    } catch(e) {
        console.error(e)
    }
}

function getRandomNumber(min, max) {
    try {
        const rn = require('random-number');
        const gen = rn.generator({
            min:  min
            , max:  max
            , integer: true
        })
        const value = gen();
       return value;
    } catch (err) {
        console.log(err);
    }
}

function helpCommand(arguments, recievedMessage) {
    if (arguments.length === 0) {
        recievedMessage.channel.send("You can use following commands: \n `!joke` \n `!chuck` \n `!toss` \n `!roll` \n `!meme` \n `!tpb` \n `!movie` \n `!warzone` \n `!play` \n `!stop` \n `!skip` \n `!vote` \n `!teams` \n `!csgo` \n  For further info on command type `!help command` ");
    } else if (arguments.length > 1) {
        recievedMessage.channel.send("Please type one command you need help with, e.g. `!help pong`")
    } else if (arguments[0] == "chuck") {
        recievedMessage.channel.send("Tells you Chuck Norris joke");
    } else if (arguments[0] == "joke") {
        recievedMessage.channel.send("Tells you random joke");
    } else if (arguments[0] == "roll") {
        recievedMessage.channel.send("Random number between 1 and 100");
    } else if (arguments[0] == "toss") {
        recievedMessage.channel.send("Coin toss");
    } else if (arguments[0] == "movie") {
        recievedMessage.channel.send("Recommends random movie.");
    } else if (arguments[0]== "meme") {
        recievedMessage.channel.send("https://api.imgflip.com/popular_meme_ids");
    } else if (arguments[0]== "tpb") {
        recievedMessage.channel.send("`!tpb <search query>`");
    } else if (arguments[0]== "play") {
        recievedMessage.channel.send("`!play <youtube url>` \n You can add multiple songs in queue and than skip them with \n `!skip`");
    } else if (arguments[0]== "!stop") {
        recievedMessage.channel.send("`!stop` \n Stops Elhator music stream");
    } else if (arguments[0]== "skip") {
        recievedMessage.channel.send("`!skip` \n Skips to next song");
    } else if (arguments[0]== "warzone") {
        recievedMessage.channel.send("`!warzone <Name#number>` \n Shows info for that user");
    } else if (arguments[0]== "vote") {
        recievedMessage.channel.send("`!vote normal` \n Start vote with `!vote small` or `!vote normal` and after that chose number and put it next to vote, eg `!vote 1`");
    } else if (arguments[0]== "csgo") {
        recievedMessage.channel.send("`!csgo <steam id>` \n Shows stats for given steam id.");
    } else if (arguments[0]== "teams") {
        recievedMessage.channel.send("`!teams` \n Splits people into 2 teams from all voice chats.");
    } else {
        recievedMessage.channel.send(`There is no command like ${arguments} try \`!help\` command to see options `);
    }
}

function warzone(args, recievedMessage) {
    const username = args[0].split('#')[0]
    CODAPI.MWwz(args[0]).then(data => {
        const br = data.br_all
        const message = `Stats for **${username}** in all battle royales: \n Wins: ${br.wins} \n Top Five: ${br.topFive} \n Top Ten: : ${br.topTen} \n Kills: ${br.kills} \n Deaths: ${br.deaths} \n Number of Games Played: ${br.gamesPlayed} \n Total Hours Played: ${(br.timePlayed/3600).toFixed(2)} \n`;
        recievedMessage.channel.send(message)
    }).catch(e => {
        console.log(e)
    });
}

function meme(args, recievedMessage) {
    try {
        const memeId = args[0]
        args.shift()
        const text0 = args.splice(0,args.indexOf('|')).join(' ')
        args.shift()
        text1 = args.join(' ')
        
        const url = `https://api.imgflip.com/caption_image?template_id=${memeId}&text0=${text0}&text1=${text1}&username=${process.env.IMGFLIP_USER}&password=${process.env.IMGFLIP_PASS}`
        const client = request.createClient(url);
        client.post('', {}, (err, res, body) => {
            if(body.success){
                recievedMessage.channel.send('', {files: [body.data.url]});
            } else {
                recievedMessage.channel.send(body.error_message);
            }
        })
    } catch(e) {
        console.error(e)
    }
}

async function tpb(args, recievedMessage) {
    try {
        const searchResult = await PirateBay.search(`${args.join(' ')}`,{
            category: 'all',
            filter: { verified: false },
            page: 0,            
            orderBy: 'seeds', 
            sortBy: 'desc' 
        });
        const retVal = searchResult.slice(0, 5).map(r => 
            `
Name: ${r.name}
Size: ${r.size}
Seeders: ${r.seeders}
Leechers: ${r.leechers}
Upload Date: ${r.uploadDate}
Link: ${r.link}
${r.verified ? 'This uploader is verified' : ''}
            `
        )
        recievedMessage.channel.send(retVal);
    } catch (e) {
        recievedMessage.channel.send('There is error with tpb. Please try again later.');
    }
}

async function exec(args, message, serverQueue) {
    if (!args.length) return message.channel.send('Please add youtube url');
    const voiceChannel = message.member.voiceChannel;
	if (!voiceChannel) return message.channel.send('You need to be in a voice channel to play music');
	const permissions = voiceChannel.permissionsFor(message.client.user);
	if (!permissions.has('CONNECT') || !permissions.has('SPEAK')) {
		return message.channel.send('I need the permissions to join and speak in your voice channel');
	}

	const songInfo = await ytdl.getInfo(args.join(' '));
	const song = {
		title: songInfo.title,
		url: songInfo.video_url,
	};

	if (!serverQueue) {
		const queueContruct = {
			textChannel: message.channel,
			voiceChannel: voiceChannel,
			connection: null,
			songs: [],
			volume: 5,
			playing: true,
		};

		queue.set(message.guild.id, queueContruct);

		queueContruct.songs.push(song);

		try {
			const connection = await voiceChannel.join();
			queueContruct.connection = connection;
			play(message.guild, queueContruct.songs[0]);
		} catch (err) {
			console.log(err);
			queue.delete(message.guild.id);
			return message.channel.send(err);
		}
	} else {
		serverQueue.songs.push(song);
		return message.channel.send(`${song.title} has been added to the queue`);
	}
}

function play(guild, song) {
	const serverQueue = queue.get(guild.id);

	if (!song) {
		serverQueue.voiceChannel.leave();
		queue.delete(guild.id);
		return;
	}

	const dispatcher = serverQueue.connection.playStream(ytdl(song.url))
		.on('end', () => {
			serverQueue.songs.shift();
			play(guild, serverQueue.songs[0]);
		})
		.on('error', error => {
			console.error(error);
		});
	dispatcher.setVolumeLogarithmic(serverQueue.volume / 10);
}

function stop(message, serverQueue) {
    if (!message.member.voiceChannel) return message.channel.send('You have to be in a voice channel to stop the music!');
    try {
        serverQueue.songs = [];
        serverQueue.connection.dispatcher.end();
    } catch (e) {}
}

function skip(message, serverQueue) {
	if (!message.member.voiceChannel) return message.channel.send('You have to be in a voice channel to stop the music!');
	if (!serverQueue) return message.channel.send('There is no song that I could skip!');
	serverQueue.connection.dispatcher.end();
}

client.login(token);
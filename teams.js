const Discord = require('discord.js');

function teams(recievedMessage) {
  const channels = recievedMessage.guild.channels.cache.filter(c => c.type === 'voice');
  const users = []
  for (const [channelID, channel] of channels) {
    for (const [memberID, member] of channel.members) {
      users.push(member.user.username)
    }
  }
  const team1 = shuffle([...users])
  let team2 = team1.splice(0, users.length >> 1)
  teamsMessage(recievedMessage, team1, team2)
}

function shuffle(a) {
  let j, x, i;
  for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
  }
  return a;
}

function teamsMessage(recievedMessage, team1, team2) {
  const footerImage = 'https://pbs.twimg.com/profile_images/1389342981107318785/AL7Ha5E4_400x400.jpg'

  let team1Players = '\u200B' 
  team1.forEach(p => {
    team1Players += `${p}, `
  });

  let team2Players = '\u200B' 
  team2.forEach(p => {
    team2Players += `${p}, `
  });

  const embededMessage = new Discord.MessageEmbed()
    .setColor('#00FF00')
    .setTitle(`Teams!`)
    .setAuthor('Varikap', 'https://gitlab.com/uploads/-/system/user/avatar/1345757/avatar.png?width=400', 'https://gitlab.com/Varikap')
    .setThumbnail('https://pbs.twimg.com/media/CGjHe7ZUcAEYiko.png')
    .addFields(
      {
        name: 'Team 1:',
        value: team1Players
      },
      {
        name: 'Team 2:',
        value: team2Players
      }
    )
    .addField("\u200B", "If you have any issues, plese let me know [here](https://gitlab.com/Varikap/discord-bot/issues).")
    .setTimestamp()
    .setFooter("\u200B", footerImage)
    recievedMessage.channel.send(embededMessage)
}

exports.teams = teams
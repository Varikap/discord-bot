# Discord bot

`.env` variables:

```IMGFLIP_USER=<user>
IMGFLIP_PASS=<pass>
DISCORD_TOKEN=<token>
DEFINE_API_KEY=<key>
MOVIE_API_KEY=<key>
```

First run:

```npm install```

Than run bot with:

```npm run start```

const axios = require('axios').default;
const Discord = require('discord.js');

function getStats(recievedMessage, id) { 
  const url = `https://public-api.tracker.gg/v2/csgo/standard/profile/steam/${id}`
  axios.get(url, {headers: {'TRN-Api-Key' : process.env.CSGO_API}})
    .then(function(res) {
      statsMessage(recievedMessage, res.data)
    })
    .catch(function (error) {
      recievedMessage.channel.send('API is down, try later!')
      console.log(error)
    })
}

function statsMessage(recievedMessage, data) {
  const footerImage = 'https://pbs.twimg.com/profile_images/1389342981107318785/AL7Ha5E4_400x400.jpg'
  const stats = data.data.segments[0].stats
  
  const embededMessage = new Discord.MessageEmbed()
    .setColor('#00FF00')
    .setTitle(`Stats for ${data.data.platformInfo.platformUserHandle}`)
    .setAuthor('Varikap', 'https://gitlab.com/uploads/-/system/user/avatar/1345757/avatar.png?width=400', 'https://gitlab.com/Varikap')
    .setThumbnail(data.data.platformInfo.avatarUrl)
    .addFields(
      {
        name: 'Time played:',
        value: `${stats.timePlayed.displayValue}`
      },
      {
        name: 'Kills:',
        value: `${stats.kills.displayValue}`
      },
      {
        name: 'Deaths:',
        value: `${stats.deaths.displayValue}`
      },
      {
        name: 'KD:',
        value: `${stats.kd.displayValue}`
      },
      {
        name: 'Matches played:',
        value: `${stats.matchesPlayed.displayValue}`
      },
      {
        name: 'Matches won:',
        value: `${stats.wins.displayValue}`
      },
      {
        name: 'Matches lost:',
        value: `${stats.losses.displayValue}`
      },
      {
        name: 'Headshot percentage:',
        value: `${stats.headshotPct.displayValue}`
      },
      {
        name: 'All time mvps:',
        value: `${stats.mvp.displayValue}`
      },
    )
    .addField("\u200B", "If you have any issues, plese let me know [here](https://gitlab.com/Varikap/discord-bot/issues).")
    .setTimestamp()
    .setFooter("\u200B", footerImage)
    recievedMessage.channel.send(embededMessage)
}

exports.getStats = getStats
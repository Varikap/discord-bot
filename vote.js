const Discord = require('discord.js');

var voteActive = false
var activeMaps = []
var result = []
var peopleVoted = []

const footerImage = 'https://pbs.twimg.com/profile_images/1389342981107318785/AL7Ha5E4_400x400.jpg'

function vote(args, recievedMessage) {
  const author = recievedMessage.author.id
  if (voteActive) {
    const voteResult = Number.parseInt(args[0])
    if (peopleVoted.includes(author)) {
      errorMessageVotedTwice(recievedMessage)
      return;
    }
    if (Number.isInteger(voteResult)){
      if (voteResult < activeMaps.length) {
        result[voteResult] += 1
        peopleVoted.push(author)
      } else {
        errorMessageOutOfRange(recievedMessage)
      }
    } else {
      errorMessageNotNumber(recievedMessage)
    }
    
  } else {
    if (args[0] === 'normal') {
      voteActive = true
      activeMaps = ['Mirage', 'Inferno', 'Train', 'Dust 2', 'Overpass', 'Cache', 'Nuke', 'Vertigo']
      result = new Array(activeMaps.length).fill(0)
      startVote(recievedMessage)
    } else if (args[0] === 'small') {
      voteActive = true
      activeMaps = ['Lake', 'Safehouse', 'Guard', 'Elysion']
      result = new Array(activeMaps.length).fill(0)
      startVote(recievedMessage)
    } else {
      errorMessageMode(recievedMessage)
      voteActive = false
      activeMaps = []
      result = []
      peopleVoted = []
      return
    }
    peopleVoted = []
    makeMapsMessage(recievedMessage)
    return
  }
}

function startVote(recievedMessage) {
  setTimeout(() => {
    votePassedMessage(recievedMessage)
    voteActive = false
    activeMaps = []
    result = []
    peopleVoted = []
  }, 40000);
}

function votePassedMessage(recievedMessage) {
  const i = result.indexOf(Math.max(...result));

  const results = []
  activeMaps.forEach((m, i) => {
    results.push({
      name: `${i}. ${m}`,
      value: `Number of votes: ${result[i]}`
    })
  })

  const embededMessage = new Discord.MessageEmbed()
    .setColor('#00FF00')
    .setTitle(`Vote passed, ${activeMaps[i]} won!`)
    .setAuthor('Varikap', 'https://gitlab.com/uploads/-/system/user/avatar/1345757/avatar.png?width=400', 'https://gitlab.com/Varikap')
    .setThumbnail('https://cdn.cloudflare.steamstatic.com/steamcommunity/public/images/avatars/0d/0d9e88ccabbe6b9ff92e8009f9b42088dbe0e9fb_full.jpg')
    .addFields(
      results
    )
    .addField("\u200B", "If you have any issues, plese let me know [here](https://gitlab.com/Varikap/discord-bot/issues).")
    .setTimestamp()
    .setFooter("\u200B", footerImage)
    recievedMessage.channel.send(embededMessage)
}

function errorMessageMode(recievedMessage) {
  const embededMessage = new Discord.MessageEmbed()
    .setColor('#FF0000')
    .setTitle('Error')
    .setAuthor('Varikap', 'https://gitlab.com/uploads/-/system/user/avatar/1345757/avatar.png?width=400', 'https://gitlab.com/Varikap')
    .setThumbnail('https://i.kym-cdn.com/entries/icons/original/000/017/318/angry_pepe.jpg')
    .addFields(
      {
        name: "\u200B",
        value: `There is only **normal** or **small** maps, chose one.`
      },
      {
        name: "\u200B",
        value: `**!vote normal** or **!vote small**`
      }
    )
    .addField("\u200B", "If you have any issues, plese let me know [here](https://gitlab.com/Varikap/discord-bot/issues).")
    .setTimestamp()
    .setFooter("\u200B", footerImage)
    recievedMessage.channel.send(embededMessage)
}

function errorMessageVotedTwice(recievedMessage) {
  const embededMessage = new Discord.MessageEmbed()
    .setColor('#FF0000')
    .setTitle('Error')
    .setAuthor('Varikap', 'https://gitlab.com/uploads/-/system/user/avatar/1345757/avatar.png?width=400', 'https://gitlab.com/Varikap')
    .setThumbnail('https://image.pngaaa.com/619/619-middle.png')
    .addFields(
      {
        name: "\u200B",
        value: `Can't vote twice, ${recievedMessage.author.username}.`
      }
    )
    .addField("\u200B", "If you have any issues, plese let me know [here](https://gitlab.com/Varikap/discord-bot/issues).")
    .setTimestamp()
    .setFooter("\u200B", footerImage)
    recievedMessage.channel.send(embededMessage)
}

function errorMessageOutOfRange(recievedMessage) {
  const embededMessage = new Discord.MessageEmbed()
    .setColor('#FF0000')
    .setTitle('Error')
    .setAuthor('Varikap', 'https://gitlab.com/uploads/-/system/user/avatar/1345757/avatar.png?width=400', 'https://gitlab.com/Varikap')
    .setThumbnail('https://i.kym-cdn.com/entries/icons/original/000/017/318/angry_pepe.jpg')
    .addFields(
      {
        name: "\u200B",
        value: `Out of range, please select from 0 to ${activeMaps.length - 1}, ${recievedMessage.author.username}.`
      }
    )
    .addField("\u200B", "If you have any issues, plese let me know [here](https://gitlab.com/Varikap/discord-bot/issues).")
    .setTimestamp()
    .setFooter("\u200B", footerImage)
    recievedMessage.channel.send(embededMessage)
}

function errorMessageNotNumber(recievedMessage) {
  const embededMessage = new Discord.MessageEmbed()
    .setColor('#FF0000')
    .setTitle('Error')
    .setAuthor('Varikap', 'https://gitlab.com/uploads/-/system/user/avatar/1345757/avatar.png?width=400', 'https://gitlab.com/Varikap')
    .setThumbnail('https://image.pngaaa.com/619/619-middle.png')
    .addFields(
      {
        name: "\u200B",
        value: `Please use number when voting, ${recievedMessage.author.username}.`
      }
    )
    .addField("\u200B", "If you have any issues, plese let me know [here](https://gitlab.com/Varikap/discord-bot/issues).")
    .setTimestamp()
    .setFooter("\u200B", footerImage)
    recievedMessage.channel.send(embededMessage)
}

function makeMapsMessage(recievedMessage) {
  const voteMaps = activeMaps.map((m, i) => { 
    return {
      name: "\u200B",
      value: `**${i}.  ${m}**`
    }
   })
  const embededMessage = new Discord.MessageEmbed()
    .setColor('#0099ff')
    .setTitle('Vote started')
    .setAuthor('Varikap', 'https://gitlab.com/uploads/-/system/user/avatar/1345757/avatar.png?width=400', 'https://gitlab.com/Varikap')
    .setDescription("To vote, chose map number and put it next to \`!vote\`. For example \`!vote 1\`")
    .setThumbnail('https://img-9gag-fun.9cache.com/photo/a05Z52d_460s.jpg')
    .addFields(
      voteMaps
    )
    .addField("\u200B", "If you have any issues, plese let me know [here](https://gitlab.com/Varikap/discord-bot/issues).")
    .setTimestamp()
    .setFooter("\u200B", footerImage)
    recievedMessage.channel.send(embededMessage)
}

exports.vote = vote